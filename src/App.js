import React, {Component} from 'react';
import './App.less';
import TodoList from "./components/TodoList";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonName: 'show',
      value: '',
      todoVisible: 'hidden'
    }
    this.showTodoList = this.showTodoList.bind(this);
    this.refresh = this.refresh.bind(this);
  }

  showTodoList(e) {
    this.setState(
      {
        todoVisible: this.state.todoVisible == 'hidden' ? 'visible' : 'hidden',
        buttonName: this.state.buttonName == 'show' ? 'hidden' : 'show'
      }
    )
  }

  refresh(){
    window.location.reload();
  }

  render() {
    return (
      <div className='App'>
        <button onClick={this.showTodoList}>{this.state.buttonName}</button>
        <button onClick={this.refresh}>Refresh</button>
        <TodoList className={this.state.todoVisible}/>
      </div>
    )
  }
}

export default App;