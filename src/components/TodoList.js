import React, {Component} from 'react';
import './todolist.less';

class TodoList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      liCount: 0,
      numbers: []
    }
    this.addList = this.addList.bind(this);
  }

  addList(event) {
    this.setState({
      liCount: this.state.liCount + 1,
      numbers: []
    })
  }

  componentDidMount() {
    console.log("componentDidMount");
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log("componentDidUpdate");

  }

  componentWillUnmount() {
    console.log("componentWillUnmount");

  }

  render() {
    let numbers = [];
    for (let i = 0; i < this.state.liCount; i++) {
      numbers.push(i);
    }
    const todoItems = numbers.map((index) =>
      <li>
        <input type={"text"} value={"List Title"+index} key={'todo'+index.toString()}/>
      </li>
    );
    console.log("render");
    return (
      <div className={this.props.className}>
        <button onClick={this.addList}>Add</button>
        <ul>
          {todoItems}
        </ul>
      </div>
    );
  }
}

export default TodoList;

